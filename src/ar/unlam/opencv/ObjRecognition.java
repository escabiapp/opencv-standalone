package ar.unlam.opencv;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class ObjRecognition extends Application
{
	private static final String OPENCV_LIB_PATH = "/usr/local/Cellar/opencv3/3.1.0_3/share/OpenCV/java/libopencv_java310.so";

	/**
	 * The main class for a JavaFX application. It creates and handles the main
	 * window with its resources (style, graphics, etc.).
	 * 
	 * This application looks for any tennis ball in the camera video stream and
	 * try to select them according to their HSV values. Found tennis balls are
	 * framed with a blue line.
	 * 
	 * @author <a href="mailto:morrongiellomartin@gmail.com">Martin Morrongiello</a>
	 * @version 1.1 (2015-11-26)
	 * @since 1.0 (2015-01-13)
	 * 
	 */
	@Override
	public void start(Stage primaryStage)
	{
		try
		{
			// load the FXML resource
			BorderPane root = (BorderPane) FXMLLoader.load(getClass().getResource("ObjRecognition.fxml"));
			// set a whitesmoke background
			root.setStyle("-fx-background-color: whitesmoke;");
			// create and style a scene
			Scene scene = new Scene(root, 1000, 600);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			// create the stage with the given title and the previously created
			// scene
			primaryStage.setTitle("Object Recognition");
			primaryStage.setScene(scene);
			// show the GUI
			primaryStage.show();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args)
	{
		// load the native OpenCV library
		System.load(OPENCV_LIB_PATH);
		
		launch(args);
	}
}
